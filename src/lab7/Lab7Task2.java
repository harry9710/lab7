/*2. Using bubble sort algorithm, sort strings from a list of strings alphabetically basing on first letter.*/

package lab7;
import java.util.Arrays;

public class Lab7Task2 {
	public static void main(String[] args) {
		String str[] = { "piano", "cat", "dog", "computer", "book" };
		int n = str.length;
		
		for (int i = 0; i < n-1; i++) {
			for (int j = 0; j < n-i-1; j++) {				
				if (str[j+1].compareTo(str[j]) < 0) {	//comparing strings
					
					//swap strings
					String temp = str[j];
					str[j] = str[j+1];
					str[j+1] = temp;
				}
			}
		}
		System.out.println("Sorted strings: " + Arrays.toString(str));
	}
}
