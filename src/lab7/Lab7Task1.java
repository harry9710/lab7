/*1.	Write an implementation of selection sort algorithm, that sorts integers in an ascending way
a.	The list of integers is passed as an array taken from console or input file.*/

package lab7;
import java.util.*;

public class Lab7Task1 {
	public static void main(String[] args) {
		//input an array
		Scanner input = new Scanner(System.in);
		System.out.println("Number of elements in the array:");
		int n = input.nextInt();
		int[] arr = new int[n];
		System.out.println("Enter all of elements:");
		for (int i = 0; i < n; i++) {
			arr[i] = input.nextInt();
		}
		
		//one by one move boundary of unsorted subarray  
		for (int i = 0; i < n-1; i++) {
			
			//find the minimum element in unsorted array  
			int min_inx = i;
			for (int j = i+1; j < n; j++) {
				if (arr[j] < arr[min_inx]) 
					min_inx = j;
				
			//swap the found minimum element with the first element  
			int temp = arr[min_inx];
			arr[min_inx] = arr[i];
			arr[i] = temp;
			}
		}
		
		//print out the sorted array
		System.out.println(Arrays.toString(arr));
		
	}

}
